/*
 * ECM BTB - Equipe ECM - CGI 2016
 * 
 */
package fr.cgi.atc.tools.corruptedpdfchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * CorruptedPDFChecker.java DOCUMENTEZ_MOI
 * 
 * @author viollinn Date: 25 oct. 2016
 */
public class CorruptedPDFChecker {

	private static final Logger LOG = Logger.getLogger(CorruptedPDFChecker.class);

	/**
	 * Constructeur de la classe CorruptedPDFChecker.java
	 *
	 */
	public CorruptedPDFChecker(String dir) {

		File[] pdfFiles = new File(dir).listFiles(new FileFilter() {

			public boolean accept(File file) {
				return file.getName().toLowerCase().endsWith("pdf");
			}
		});

		for (int i = 0; i < pdfFiles.length; i++) {

			File file = pdfFiles[i];

			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				String firstLine = br.readLine();
				if (!firstLine.substring(0, 5).equals("%PDF-")) {
					LOG.error("File [" + file.getAbsolutePath() + "] is corrupted");
				}
			} catch (FileNotFoundException e) {
				LOG.error("Erreur de chargement du fichier [" + file.getAbsolutePath() + "]");
			} catch (IOException e) {
				LOG.error("Erreur de lecture du fichier [" + file.getAbsolutePath() + "]");
			}

		}

	}

	/**
	 * DOCUMENTEZ_MOI
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		if ((args == null) || (args.length == 0)) {
			LOG.error("Il manque un répertoire en paramètre");
		} else {
			new CorruptedPDFChecker(args[0]);
		}
	}

}
